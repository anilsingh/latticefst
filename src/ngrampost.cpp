/* 
 * File:   ngrampost.cpp
 * Author: Francois Yvon and Anil Kumar Singh
 *
 * Created on November 30, 2012, 2:53 AM
 */
#include "ngrampost.h"

/*
 * 
 */
bool readOrCompileFST(VectorFst<LogArc> **logFst, string& pathOrPrefix, bool compile, bool acceptor)
{
    if(compile)
    {
        compileFST(pathOrPrefix, acceptor);
        
//        string compiledPath = pathOrPrefix + ".fst";
        
        readOrCompileFST(logFst, pathOrPrefix, false, false);
    }
    else
    {
//        string fstPath = pathOrPrefix;
        string fstPath = pathOrPrefix + ".fst";
        
        if (fstPath.length() == 0 || (*logFst = VectorFst< LogArc >::Read(fstPath)) == NULL) {
            cerr << "Could not load the n-gram FST from " << fstPath << endl;
            abort();
        } else {
            cout << "Loaded the n-gram FST from " << fstPath << endl;
            
//            string test = pathOrPrefix + ".tst";
//            
//            logFst.Write(test);
        }        
    }
}

bool compileFST(string& pathPrefix, bool acceptor)
{
    string source = pathPrefix + ".fst.txt";
    string dest = pathPrefix + ".fst";
    
    string isymbols = pathPrefix + ".input.syms";
    string osymbols = pathPrefix + ".output.syms";
    
    istream *istrm = new ifstream(source.c_str());

    if (!*istrm) {
        cout << ": Open failed, file = " << source << endl;
        return 1;
    }

    const SymbolTable *isyms = 0, *osyms = 0, *ssyms = 0;

//    isyms = SymbolTable::ReadText(FLAGS_isymbols, FLAGS_allow_negative_labels);
    isyms = SymbolTable::ReadText(isymbols, false);

    if (!isyms) exit(1);

    osyms = SymbolTable::ReadText(osymbols, false);

    if (!osyms) exit(1);

//    ssyms = SymbolTable::ReadText(FLAGS_ssymbols);
//
//    if (!ssyms) exit(1);
    
    string fstType = "vector";
//    string arcType = "standard";
    string arcType = "log";

    CompileFst(*istrm, source, dest, fstType, arcType,
            isyms, osyms, ssyms, acceptor, true, true,
            false, false);

    cout << "Compiled the FST to: " << dest << endl;

    if (istrm != &std::cin)
        delete istrm;    
}

bool composeNormalizeComputePosteriorsAllNGrams(string& pathPrefix, bool compose,
        bool normalize, bool save, bool compile, bool acceptor)
{
    VectorFst<LogArc> *latticeFst;

    if(compose)
    {
        string latticeFile = pathPrefix.c_str();

        readOrCompileFST(&latticeFst, latticeFile, compile, acceptor);
    }
    
    cout << "Processing unigram FST..." << endl;

    composeNormalizeComputePosteriors(*latticeFst, 1, pathPrefix, false,
        normalize, save, compile, acceptor);

    cout << "Processing bigram FST..." << endl;

    composeNormalizeComputePosteriors(*latticeFst, 2, pathPrefix, compose,
        normalize, save, compile, acceptor);

    cout << "Processing trigram FST..." << endl;

    composeNormalizeComputePosteriors(*latticeFst, 3, pathPrefix, compose,
        normalize, save, compile, acceptor);

    cout << "Processing four gram FST..." << endl;

    composeNormalizeComputePosteriors(*latticeFst, 4, pathPrefix, compose,
        normalize, save, compile, acceptor);
}

bool composeNormalizeComputePosteriors(int order, string& pathPrefix, bool compose,
        bool normalize, bool save, bool compile, bool acceptor)
{   
    VectorFst<LogArc> *latticeFst;
    
    if(order == 1)
    {
        compose = false;
    }

    if(compose)
    {
        string latticeFile = pathPrefix.c_str();

        readOrCompileFST(&latticeFst, latticeFile, compile, acceptor);
    }
    
    composeNormalizeComputePosteriors(*latticeFst, order, pathPrefix, compose,
        normalize, save, compile, acceptor);
}

bool composeNormalizeComputePosteriors(VectorFst<LogArc>& latticeFst, int order, string& pathPrefix,
        bool compose, bool normalize, bool save, bool compile, bool acceptor)
{
    VectorFst<LogArc> *ngramFst;
    VectorFst<LogArc> *normalizedFst;

    AMap alphas;

    NMap ngramAlphas;

    AMap posteriors;
    
    if(normalize)
    {
        normalizedFst = new VectorFst<LogArc>();        
    }
    
    if(order == 1 && normalize)
    {
        ngramFst = &latticeFst;
        
        normalizeFST(*ngramFst, *normalizedFst);
    }    
    else if(order > 1 && normalize)
    {
        if(compose)
        {
            string ngLatticeFile = getPathPrefix(pathPrefix, order);

            readOrCompileFST(&ngramFst, ngLatticeFile, compile, acceptor);            
        }
        
        if(compose && normalize)
        {
            composeNormalize(latticeFst, *ngramFst, *normalizedFst, pathPrefix, save);
        }
        else if(normalize)
        {
            normalizeFST(*ngramFst, *normalizedFst);            
        }
    }
    else /* If already normalizd */
    {
        string normalizedLatticeFile = getPathPrefix(pathPrefix, order) + ".norm";

        readOrCompileFST(&normalizedFst, normalizedLatticeFile, compile, acceptor);
    }
    
    computePosteriors(*normalizedFst, alphas, ngramAlphas, posteriors);
    
    string ngPathPrefix = getPathPrefix(pathPrefix, order);

    saveResults(alphas, ngramAlphas, posteriors, *normalizedFst, ngPathPrefix, save);    
}

bool composeNormalize(VectorFst<LogArc>& latticeFst,
        VectorFst<LogArc>& ngramFst, VectorFst<LogArc>& normalizedFst, string& pathPrefix, bool save) {
    
    ArcSort(&latticeFst, OLabelCompare<LogArc>());
    ArcSort(&ngramFst, ILabelCompare<LogArc>());
    
//    ArcSortFst<LogArc, OLabelCompare<LogArc> >(latticeFst, OLabelCompare<LogArc>());    
//    ArcSortFst<LogArc, ILabelCompare<LogArc> >(ngramFst, ILabelCompare<LogArc>());    
    
    VectorFst<LogArc> ngramLatticeFst;

    cout << "Composing the lattice FST with the ngram FST..." << endl;

    ngramLatticeFst = ComposeFst<LogArc > (latticeFst, ngramFst);

    bool acyclic = normalizeFST(ngramLatticeFst, normalizedFst);

    return acyclic;
}

bool normalizeFST(VectorFst<LogArc>& ngramLatticeFst, VectorFst<LogArc>& normalizedFst)
{
    cout << "Projecting on output..." << endl;
    Project<LogArc > (&ngramLatticeFst, PROJECT_OUTPUT);

    cout << "Removing epsilons..." << endl;
    RmEpsilon(&ngramLatticeFst);

//    VectorFst<LogArc> determinizedFst;
    
    cout << "Determinizing..." << endl;
    Determinize<LogArc > (ngramLatticeFst, &normalizedFst);
//    Determinize<LogArc > (ngramLatticeFst, &determinizedFst);
    
    cout << "Minimizing..." << endl;
    Minimize<LogArc >(&normalizedFst, NULL, kDelta);
//    Minimize<LogArc >(&determinizedFst, &normalizedFst, kDelta);

    cout << "Pushing weights..." << endl;
    Push<LogArc > (&normalizedFst,
           REWEIGHT_TO_INITIAL , kDelta, true);

    cout << "Topologically sorting the FST..." << endl;
    bool acyclic = TopSort(&normalizedFst);
    
    return acyclic;
}

int getNumArcs(VectorFst<LogArc>& fst, bool output)
{
    int numArcs = 0;
    
    AMap arcMap;
    
    for (StateIterator<VectorFst<LogArc> > siter(fst); !siter.Done(); siter.Next()) {
 
        LogArc::StateId stateId = siter.Value();
    
        for (ArcIterator<VectorFst<LogArc> > aiter(fst, stateId); !aiter.Done(); aiter.Next()) {

            const LogArc &arc = aiter.Value();

            if(output)
            {
                if(arcMap.find(arc.olabel) == arcMap.end())
                {
                        arcMap[arc.olabel] == 1;
                        numArcs++;
                }
            }
            else
            {
                if(arcMap.find(arc.ilabel) == arcMap.end())
                {
                        arcMap[arc.ilabel] == 1;
                        numArcs++;
                }
            }
        }
    }
    
    return numArcs;
}

int computePosteriors(VectorFst<LogArc>& normalizedFst, AMap& alphas, NMap& ngramAlphas, 
        map<int, LogWeight>& posteriors) {
    
    LogArc::StateId numStates = normalizedFst.NumStates();

    const SymbolTable* inputSymbols = normalizedFst.InputSymbols();
    
    int numArcs = getNumArcs(normalizedFst, false);

    // Initializations
    for (StateIterator<VectorFst<LogArc> > siter(normalizedFst); !siter.Done(); siter.Next()) {
 
        LogArc::StateId stateId = siter.Value();

        for (ArcIterator<VectorFst<LogArc> > aiter(normalizedFst, stateId); !aiter.Done(); aiter.Next()) {

            const LogArc &arc = aiter.Value();

            posteriors[arc.ilabel] = LogArc::Weight::Zero();    
        }
    }

    for (int is = 0; is < numStates; is++) {
        if(is == normalizedFst.Start())
        {
            alphas[is] = LogArc::Weight::One();                        
        }
        else
        {
            alphas[is] = LogArc::Weight::Zero();                        
        }
    }

    for (StateIterator<VectorFst<LogArc> > siter(normalizedFst); !siter.Done(); siter.Next()) {
 
        LogArc::StateId stateId = siter.Value();

        LogWeight fw = normalizedFst.Final(stateId);

        LogWeight fwi = fw;
        
//        if(fwi == LogArc::Weight::Zero())
//        {
//            fwi = LogArc::Weight::One();
//        }
    
        for (ArcIterator<VectorFst<LogArc> > aiter(normalizedFst, stateId); !aiter.Done(); aiter.Next()) {

            const LogArc &arc = aiter.Value();
           
            LogWeight w = arc.weight;

            LogArc::StateId nextStateId = arc.nextstate;
                
//            if(w == LogArc::Weight::Zero())
//            {
//                w = LogArc::Weight::One();
//            }
                
            LogWeight a = alphas[stateId];
            LogWeight an = alphas[nextStateId];
            LogWeight aw = Times(a, w);
            an = Plus(an, aw);
            
            alphas[nextStateId] = an;
            
            if (ngramAlphas[nextStateId].find(arc.ilabel) == ngramAlphas[nextStateId].end())
            {
                ngramAlphas[nextStateId][arc.ilabel] = LogArc::Weight::Zero();
            }
            
            LogWeight ngan = ngramAlphas[nextStateId][arc.ilabel];
            ngan = Plus(ngan, aw);
            
            ngramAlphas[nextStateId][arc.ilabel] = ngan;
                       
            for (AMapItr itr = ngramAlphas[stateId].begin(); itr != ngramAlphas[stateId].end(); itr++) {
                int arcILabel = itr->first;

                if(arcILabel == arc.ilabel)
                {
                    continue;
                }

                if(ngramAlphas[nextStateId].find(arcILabel) == ngramAlphas[nextStateId].end())
                {
                    ngramAlphas[nextStateId][arcILabel] = LogArc::Weight::Zero();
                }

                LogWeight nga = ngramAlphas[stateId][arcILabel];
                LogWeight nganp = ngramAlphas[nextStateId][arcILabel];
                LogWeight ngapw = Times(nga, w);
                nganp = Plus(nganp, ngapw);
                
                ngramAlphas[nextStateId][arcILabel] = nganp;                
            }
        }
            
        // Updating posteriors
        if (normalizedFst.NumArcs(stateId) == 0
                || normalizedFst.Final(stateId) != LogArc::Weight::Zero()) {

            for (AMapItr itr = ngramAlphas[stateId].begin(); itr != ngramAlphas[stateId].end(); itr++) {

                int arcILabel = itr->first;

                LogWeight p = posteriors[arcILabel];
                LogWeight ngaf = ngramAlphas[stateId][arcILabel];
                LogWeight pw = Times(ngaf, fwi);
                p = Plus(p, pw);

                posteriors[arcILabel] = p;
            }                        
        }
    }

    cout << "Finished calculating posteriors..." << endl;

    return 0;    
}

int saveResults(AMap& alphas, NMap& ngramAlphas, AMap& posteriors, VectorFst<LogArc>& normalizedFst,
        string& pathPrefix, bool saveNormFst)
{
    if(saveNormFst)
    {
        string savePath = pathPrefix + ".norm.fst";
        cout << "Saving the normalized FST to:" << savePath << endl;
        normalizedFst.Write(savePath);    
    }

    string alphaFile = pathPrefix + ".alphas";

    printAlphas(alphas, normalizedFst, alphaFile);

    string ngramAlphaFile = pathPrefix + ".ngram.alphas";

    printNGramAlphas(ngramAlphas, normalizedFst, ngramAlphaFile);

    string postFile = pathPrefix + ".post";

    printPosteriors(posteriors, normalizedFst, postFile);    
}

int printPosteriors(AMap& posteriors, VectorFst<LogArc>& normalizedFst, string& path) {
    
    ofstream outputFile(path.c_str());
    
    const SymbolTable* inputSymbolsFinal = normalizedFst.InputSymbols();
    
    cout << "Number of ngrams: " << posteriors.size() << endl;

    for (AMapItr itr = posteriors.begin(); itr != posteriors.end(); itr++) {
        int arcILabel = itr->first;

        outputFile << inputSymbolsFinal->Find(arcILabel) << "\t" << posteriors[arcILabel] << "\t" << exp(-posteriors[arcILabel].Value()) << endl;
    }    
           
    outputFile.close();

    return 0;
}

int printAlphas(AMap& alphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix) {
        
    ofstream outputFile(pathPrefix.c_str());
    
    int numStates = normalizedFst.NumStates();
    
    cout << "Number of states: " << numStates << endl;

    for (AMapItr itr = alphas.begin(); itr != alphas.end(); itr++) {
        int stateId = itr->first;

        outputFile << stateId << "\t" << alphas[stateId] << "\t" << exp(-alphas[stateId].Value()) << endl;
    }    
           
    outputFile.close();

    return 0;
}

int printNGramAlphas(NMap& ngramAlphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix) {
    
    ofstream outputFile(pathPrefix.c_str());
    
    const SymbolTable* inputSymbolsFinal = normalizedFst.InputSymbols();
    
    for (NMapItr nitr = ngramAlphas.begin(); nitr != ngramAlphas.end(); nitr++) {
        int stateId = nitr->first;
        
        AMap ngalpha = nitr->second;

        for (AMapItr aitr = ngalpha.begin(); aitr != ngalpha.end(); aitr++) {
            int arcILabel = aitr->first;

            outputFile << stateId << "\t" << inputSymbolsFinal->Find(arcILabel) << "\t" << ngalpha[arcILabel] << "\t" << exp(-ngalpha[arcILabel].Value()) << endl;
        }
    }    
           
    outputFile.close();

    return 0;
}

string getPathPrefix(string& pathPrefix, int order)
{
    string ngSuffix = "";

    if(order == 2)
    {
        ngSuffix = ".bg";
    }
    else if(order == 3)
    {
        ngSuffix = ".tg";
    }
    else if(order == 4)
    {
        ngSuffix = ".fg";
    }

    string extPathPrefix = pathPrefix + ngSuffix;
    
    return extPathPrefix;    
}

 