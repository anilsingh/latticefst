/* 
 * File:   ngrampost.cpp
 * Author: Francois Yvon and Anil Kumar Singh
 *
 * Created on November 30, 2012, 2:53 AM
 * This is a temptative implemantation of De Gispert et al, 2012 paper
 * N-gram posterior probability confidence measures for statistical machine translation: an empirical study
 * Mach Translat, DOI 10.1007/s10590-012-9132-2
 */

#include <ngrampost.h>

/// \function composeNormalizeComputePosteriorsAllNGram
/// Loads the lattice, if needed, then compute the posteriors for all n-gram orders
/// \bug: compose should not be an argument
///       max-order should be an argument
bool composeNormalizeComputePosteriorsAllNGrams(string& pathPrefix,
						bool compose,
						bool normalize,
						bool save,
						bool compile,
						bool acceptor)
{
  VectorFst<LogArc> *latticeFst;
  
  string latticeFile = pathPrefix.c_str();
  readOrCompileFST(&latticeFst, latticeFile, compile, acceptor);
  
  compose = false;
  for (int order=1; order <5; order++) {
    cout << "Processing" << order <<  "-gram FST..." << endl;
    if (composeNormalizeComputePosteriors(*latticeFst, order, 
					  pathPrefix, compose,
					  normalize, save,
					  compile, acceptor) == false) return false;
    /// except for 1-grams, we need to perform n-gram expansion
    /// by composing with n-gram files
    compose = true; 
  }
  
  return true;
}

/// \function readOrCompile
/// Depending on the options, either read a compiled FST or a text file. In the latter
/// case, it performs compilation for you as well
/// \return true unless something went badly wrong (it wont return at all then)
bool readOrCompileFST(VectorFst<LogArc> **logFst,
		      string& pathOrPrefix,
		      bool compile,
		      bool acceptor)
{
  if (compile)
    doCompileFST(pathOrPrefix, acceptor);
  
  /// Now it is compiled, I can load it
  string fstPath = pathOrPrefix + ".fst";
  
  if (fstPath.length() == 0 || (*logFst = VectorFst< LogArc >::Read(fstPath)) == NULL) {
    cerr << "Could not load the n-gram FST from " << fstPath << endl;
    abort();
  } 
  else {
    cout << "Loaded the n-gram FST from " << fstPath << endl;
    //            string test = pathOrPrefix + ".tst";
    //            logFst.Write(test);
  }        
  return true;
}

///  \function doCompileFST
///  Loads a text file and subsequent symbol tables, produce a compiled version
/// \args :
///  - path of the text file (assumed extension .fst.txt)
///  - flag : transducer or acceptor
/// \return: true on success, false otherwise
/// \fixme: file names
/// 
bool doCompileFST(string& pathPrefix, bool acceptor)
{
    string source   = pathPrefix + ".fst.txt";
    string dest     = pathPrefix + ".fst";
    string isymbols = pathPrefix + ".input.syms";
    string osymbols = pathPrefix + ".output.syms";
    
    istream *istrm = new ifstream(source.c_str());
    if (!*istrm) {
        cout << ": Open failed, file = " << source << endl;
        return false;
    }

    /// --- read symbol files
    const SymbolTable *isyms = 0, *osyms = 0, *ssyms = 0;
    isyms = SymbolTable::ReadText(isymbols, false);
    if (!isyms) {
        cout << ": Failed to open input symbol file = " << isymbols << endl;
        exit(1);
    }
    osyms = SymbolTable::ReadText(osymbols, false);
    if (!osyms) {
      cout << ": Failed to open output symbol file = " << osymbols << endl;
      exit(1);
    }
    /// Do compile
    CompileFst(*istrm, source, dest, string("vector"), string("log"),
            isyms, osyms, ssyms, acceptor, true, true, false, false);

    cout << "Compiled the FST to: " << dest << endl;
    if (istrm != &std::cin)
        delete istrm;    

    return true;
}
/// \function composeNormalizeComputePosteriors
/// Actual computation of posteriors in three steps
/// 1. ngram expansion (if needed)
/// 2. normalization (if needed)
/// 3. posterior computation
/// \args:
/// - the input word lattice
/// - the order of ngram
/// - a path to load/save files
/// - flag: do we need to compose ?
/// - flag: do we need to normalize
/// - flag: do we need to save stuff ?
bool composeNormalizeComputePosteriors(VectorFst<LogArc>& latticeFst, 
				       int order, string& pathPrefix,
				       bool compose, bool normalize, 
				       bool save, bool compile, bool acceptor)
{
    VectorFst<LogArc> * ngramFst;
    VectorFst<LogArc> * normalizedFst;

    AMap alphas;       /// This stores the \alpha[q]   (one per state) formally a map<int, LogWeight> AMap;
    NMap ngramAlphas;  /// This stores the \alpha[q,u] (one per state and n-gram) formally a map<int, AMap> NMap;    
    AMap posteriors;   /// This stores the posteriors  (one per n-gram) a map<int, LogWeight> AMap;
    
    if (normalize) {
      normalizedFst = new VectorFst<LogArc>();        
      
      if (order == 1) {
	ngramFst = &latticeFst;
	normalizeFST(*ngramFst, *normalizedFst);
      }
      else if (compose)
	{
	  string ngLatticeFile = getPathPrefix(pathPrefix, order);
	  readOrCompileFST(&ngramFst, ngLatticeFile, compile, acceptor);
	  composeNormalize(latticeFst, *ngramFst, *normalizedFst, pathPrefix, save);
	}
      else
	normalizeFST(*ngramFst, *normalizedFst);            
    }
    else /* If already normalized */
    {
      string normalizedLatticeFile = getPathPrefix(pathPrefix, order) + ".norm";
      readOrCompileFST(&normalizedFst, normalizedLatticeFile, compile, acceptor);
    }
    
    /// Ready to compute the posterior on the expanded/normalized lattices
    computePosteriors(*normalizedFst, alphas, ngramAlphas, posteriors);
    string ngPathPrefix = getPathPrefix(pathPrefix, order);

    saveResults(alphas, ngramAlphas, posteriors, *normalizedFst, ngPathPrefix, save);    
}

/// \function composeNormalize
/// Anciliary function, that just does n-gram expansion + normalization
bool composeNormalize(VectorFst<LogArc>& latticeFst,
		      VectorFst<LogArc>& ngramFst, 
		      VectorFst<LogArc>& normalizedFst, 
		      string& pathPrefix, bool save) 
{
  /// A requirement for composition, some arcs should be sorted
  /// Note it is actually unnecessary to sort on both tapes
  ArcSort(&latticeFst, OLabelCompare<LogArc>());
  ArcSort(&ngramFst, ILabelCompare<LogArc>());
  
  VectorFst<LogArc> ngramLatticeFst;

  cout << "Composing the lattice FST with the ngram FST..." << endl;
  ngramLatticeFst = ComposeFst<LogArc > (latticeFst, ngramFst);
  
  cout << "Projecting on output..." << endl;
  Project<LogArc > (&ngramLatticeFst, PROJECT_OUTPUT);

  bool acyclic = normalizeFST(ngramLatticeFst, normalizedFst);

  return acyclic;
}
/// \function normalizeFST
/// Path normalization thru weight pushing in the log-semi ring
/// Note that the projection onto the output should be done before composition
bool normalizeFST(VectorFst<LogArc>& ngramLatticeFst, 
		  VectorFst<LogArc>& normalizedFst)
{
    cout << "Removing epsilons..." << endl;
    RmEpsilon(&ngramLatticeFst);

    cout << "Determinizing..." << endl;
    Determinize<LogArc > (ngramLatticeFst, &normalizedFst);
    
    cout << "Minimizing..." << endl;
    Minimize<LogArc >(&normalizedFst, NULL, kDelta);

    cout << "Pushing weights..." << endl;
    Push<LogArc > (&normalizedFst, REWEIGHT_TO_INITIAL , kDelta, true);

    cout << "Topological sort the FST..." << endl;
    bool acyclic = TopSort(&normalizedFst);
    
    return acyclic;
}
/// \function computePosteriors
/// Actual computation of posteriors in three steps
/// \args:
/// - the input normalized lattice
/// - the table to return:
/// - the per state alphas
/// - the per state and ngram alphas
/// - the actual posteriors
int computePosteriors(VectorFst<LogArc>& normalizedFst, 
		      AMap& alphas, 
		      NMap& ngramAlphas, 
		      AMap& posteriors) 
{
  
  LogArc::StateId numStates = normalizedFst.NumStates();
  const SymbolTable* inputSymbols = normalizedFst.InputSymbols();
  
  /// Initializations of all ngrams that exist in the graph
  /// 
  for (StateIterator<VectorFst<LogArc> > siter(normalizedFst); !siter.Done(); siter.Next()) {
    LogArc::StateId stateId = siter.Value();
    for (ArcIterator<VectorFst<LogArc> > aiter(normalizedFst, stateId); !aiter.Done(); aiter.Next()) {
      const LogArc &arc = aiter.Value();
      posteriors[arc.ilabel] = LogArc::Weight::Zero();    /// Wait - should not this be insert() ?
    }
  }
  /// Initialize the alphas 0 everywhere execpt on first node
  for (int is = 0; is < numStates; is++) {
    alphas[is] = LogArc::Weight::Zero();
    if (is == normalizedFst.Start())
      alphas[is] = LogArc::Weight::One();                 /// Wait - should not this be insert () ?
  }

  for (StateIterator<VectorFst<LogArc> > siter(normalizedFst); !siter.Done(); siter.Next()) {
    
    LogArc::StateId stateId = siter.Value();
    
    for (ArcIterator<VectorFst<LogArc> > aiter(normalizedFst, stateId); !aiter.Done(); aiter.Next()) {
      const LogArc &arc = aiter.Value();
      LogWeight w = arc.weight;
      LogArc::StateId nextStateId = arc.nextstate;
      /// Line 3 of de Gispert and al. push the per state alpha - easy
      alphas[nextStateId] = Plus(Times(alphas[stateId], arc.weight), alphas[nextStateId]);
      
      /// This arc label has never reached nextStateId -- insert it with a null weight
      if (ngramAlphas[nextStateId].find(arc.ilabel) == ngramAlphas[nextStateId].end())
	ngramAlphas[nextStateId][arc.ilabel] = LogArc::Weight::Zero();
      /// Push perstate, per ngram alphas, this is line 6 of de Gispert and al
      ngramAlphas[nextStateId][arc.ilabel] = Plus(Times(alphas[stateId], arc.weight), ngramAlphas[nextStateId][arc.ilabel]);
      
      /// Now simply propagate the other ngrams -- loop lines 7 to 10
      for (AMapItr itr = ngramAlphas[stateId].begin(); itr != ngramAlphas[stateId].end(); itr++) {
	int arcILabel = itr->first;
	/// skip this arc's label, it has already be taken care of
	if(arcILabel == arc.ilabel) continue;
	
	if (ngramAlphas[nextStateId].find(arcILabel) == ngramAlphas[nextStateId].end())
	  ngramAlphas[nextStateId][arcILabel] = LogArc::Weight::Zero();
	
	/// this is line 10
	ngramAlphas[nextStateId][arcILabel] = Plus(ngramAlphas[nextStateId][arcILabel], 
						   Times(ngramAlphas[stateId][arcILabel],arc.weight));
      }
    }
    
    /// Updating posteriors upon reaching a final states
    if (normalizedFst.NumArcs(stateId) == 0 || normalizedFst.Final(stateId) != LogArc::Weight::Zero()) {
      
      for (AMapItr itr = ngramAlphas[stateId].begin(); itr != ngramAlphas[stateId].end(); itr++) {
	int arcILabel = itr->first;
	/// This is line 13
	posteriors[arcILabel] = Plus(posteriors[arcILabel], Times(ngramAlphas[stateId][arcILabel], normalizedFst.Final(stateId)));
      }                        
    }
  }
  cout << "Finished calculating posteriors..." << endl;
  
  return 0;    
}

/// \function saveResults
/// Anciliary function for saving the results in a file
int saveResults(AMap& alphas, NMap& ngramAlphas, AMap& posteriors, VectorFst<LogArc>& normalizedFst,
        string& pathPrefix, bool saveNormFst)
{
  if(saveNormFst)
    {
      string savePath = pathPrefix + ".norm.fst";
      cout << "Saving the normalized FST to:" << savePath << endl;
      normalizedFst.Write(savePath);    
    }
  
    string alphaFile = pathPrefix + ".alphas";
    printAlphas(alphas, normalizedFst, alphaFile);

    string ngramAlphaFile = pathPrefix + ".ngram.alphas";
    printNGramAlphas(ngramAlphas, normalizedFst, ngramAlphaFile);
    
    string postFile = pathPrefix + ".post";
    printPosteriors(posteriors, normalizedFst, postFile);

    return 0;
}
/// \function printPosteriors
/// Print the results
int printPosteriors(AMap& posteriors, VectorFst<LogArc>& normalizedFst, string& path) {
    
    ofstream outputFile(path.c_str());
    
    const SymbolTable* inputSymbolsFinal = normalizedFst.InputSymbols();
    
    cout << "Number of ngrams: " << posteriors.size() << endl;

    for (AMapItr itr = posteriors.begin(); itr != posteriors.end(); itr++) {
        int arcILabel = itr->first;

        outputFile << inputSymbolsFinal->Find(arcILabel) << "\t" << posteriors[arcILabel] << "\t" << exp(-posteriors[arcILabel].Value()) << endl;
    }    
           
    outputFile.close();

    return 0;
}
/// \function printAlphas
int printAlphas(AMap& alphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix) {
        
    ofstream outputFile(pathPrefix.c_str());
    
    int numStates = normalizedFst.NumStates();
    
    cout << "Number of states: " << numStates << endl;

    for (AMapItr itr = alphas.begin(); itr != alphas.end(); itr++) {
        int stateId = itr->first;

        outputFile << stateId << "\t" << alphas[stateId] << "\t" << exp(-alphas[stateId].Value()) << endl;
    }    
           
    outputFile.close();

    return 0;
}

int printNGramAlphas(NMap& ngramAlphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix) {
    
    ofstream outputFile(pathPrefix.c_str());
    
    const SymbolTable* inputSymbolsFinal = normalizedFst.InputSymbols();
    
    for (NMapItr nitr = ngramAlphas.begin(); nitr != ngramAlphas.end(); nitr++) {
        int stateId = nitr->first;
        
        AMap ngalpha = nitr->second;

        for (AMapItr aitr = ngalpha.begin(); aitr != ngalpha.end(); aitr++) {
            int arcILabel = aitr->first;

            outputFile << stateId << "\t" << inputSymbolsFinal->Find(arcILabel) << "\t" << ngalpha[arcILabel] << "\t" << exp(-ngalpha[arcILabel].Value()) << endl;
        }
    }    
           
    outputFile.close();

    return 0;
}

string getPathPrefix(string& pathPrefix, int order)
{
  string ngSuffix = "";
  
  if(order == 2)
    ngSuffix = ".bg";
  else if(order == 3)
    ngSuffix = ".tg";
  else if(order == 4)
    ngSuffix = ".fg";
  
  string extPathPrefix = pathPrefix + ngSuffix;
  
  return extPathPrefix;    
}
/// -------------------------------
/// Deprecated

/// \bug: a very bad name: there is already another function by the same name
bool composeNormalizeComputePosteriors(int order, string& pathPrefix, bool compose,
        bool normalize, bool save, bool compile, bool acceptor)
{   
    VectorFst<LogArc> *latticeFst;
    
    /// no need to expand on unigrams : the input is a word lattice
    if (order == 1) compose = false;

    if (compose)
      {
        string latticeFile = pathPrefix.c_str();
        readOrCompileFST(&latticeFst, latticeFile, compile, acceptor);
      }
    
    composeNormalizeComputePosteriors(*latticeFst, order, pathPrefix, compose,
        normalize, save, compile, acceptor);
}

/// A very complex way to compute the number of arcs
/// further more it is not used anywhere
/// and the arcMap is not returned...
int getNumArcs(VectorFst<LogArc>& fst, bool output)
{
    int numArcs = 0;
    
    AMap arcMap;
    
    for (StateIterator<VectorFst<LogArc> > siter(fst); !siter.Done(); siter.Next()) {
 
        LogArc::StateId stateId = siter.Value();
    
        for (ArcIterator<VectorFst<LogArc> > aiter(fst, stateId); !aiter.Done(); aiter.Next()) {

            const LogArc &arc = aiter.Value();

            if(output)
            {
                if(arcMap.find(arc.olabel) == arcMap.end())
                {
                        arcMap[arc.olabel] == 1;
                        numArcs++;
                }
            }
            else
            {
                if(arcMap.find(arc.ilabel) == arcMap.end())
                {
                        arcMap[arc.ilabel] == 1;
                        numArcs++;
                }
            }
        }
    }
    
    return numArcs;
}
