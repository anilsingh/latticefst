/* 
 * File:   ngrampostmain.h
 * Author: Francois Yvon and Anil Kumar Singh
 *
 * Created on December 6, 2012, 9:11 AM
 */

#include "ngrampost.h"
#include <getopt.h>

struct globalArgs_t {
    bool compile; /* -t option: Read text files and compile from those */
    bool acceptor; /* -a option: The FST to be compiled is an acceptor */
    int order; /* -o option: NGram order, max 4, 0 means all (1-4) */
    bool compose; /* -c option: Whether to compose the lattice FST with the ngram mapping FST*/
    bool normalize; /* -n option: Whether to normalize (if compose is true, normalize has to be true) */
    bool save; /* -s option: Whether to save the (composed and) normalized FST*/
    char *pathPrefix; /* -p option: Path prefix for the FSTs and related files (input and output)*/
} globalArgs;

static const char *optString = "tao:cnsp:h?";

static const struct option longOpts[] = {
    { "compile", no_argument, NULL, 't'},
    { "acceptor", no_argument, NULL, 'a'},
    { "order", required_argument, NULL, 'o'},
    { "compose", no_argument, NULL, 'c'},
    { "normalize", no_argument, NULL, 'n'},
    { "save", no_argument, NULL, 's'},
    { "pathPrefix", required_argument, NULL, 'p'},
    { "help", no_argument, NULL, 'h'},
    { NULL, no_argument, NULL, 0}
};

void display_usage( void )
{
    cout << "ngrampostmain - Calculate ngram posterior probabilities given a word/phrase lattice" << endl;
    cout << "USAGE:" << endl;
    cout << "ngrampostmain -o <order> -c <compose?> -n <normalize?> -p pathPrefix" << endl;
    cout << "-t: Read text files and compile from those" << endl;
    cout << "-a: The FST to be compiled is an acceptor" << endl;
    cout << "-o: NGram order, max 4, 0 means all (1-4)" << endl;
    cout << "-c: Whether to compose the lattice FST with the ngram mapping FST (y or n)" << endl;
    cout << "-n: Whether to normalize (if compose is true, normalize has to be true)" << endl;
    cout << "-s: Whether to save the (composed and) normalized FST (save file will have .norm extension)" << endl;
    cout << "-p: Path prefix for the FSTs and related files (input and output)" << endl;
    cout << "-h: Help" << endl;
    /* ... */
    exit( EXIT_FAILURE );
}

int main(int argc, char** argv) {
    
    bool hardCode = false;

    if(!hardCode)
    {
        int longIndex;

        int opt = getopt_long(argc, argv, optString, longOpts, &longIndex);

        while (opt != -1) {
            switch (opt) {
                case 'o':
                    globalArgs.order = atoi(optarg);
                    break;

                case 't':
                    globalArgs.compile = true;
                    break;

                case 'a':
                    globalArgs.acceptor = true;
                    break;

                case 'c':
                    globalArgs.compose = true;
                    break;

                case 'n':
                    globalArgs.normalize = true;
                    break;

                case 's':
                    globalArgs.save = true;
                    break;

                case 'p':
                    globalArgs.pathPrefix = optarg;
                    break;

                case 'h': /* fall-through is intentional */
                case '?':
                    display_usage();
                    break;

                default:
                    /* You won't actually get here. */
                    break;
            }

            opt = getopt_long(argc, argv, optString, longOpts, &longIndex);
        }

        cout << "Path prefix: " << globalArgs.pathPrefix << endl;

        if(globalArgs.pathPrefix == NULL)
        {
            display_usage();

            cout << "No path prefix given" << endl;

            exit(1);
        }

        cout << "Starting to calculate NGram posterior probabilities..." << endl;
        
        string pathPrefix = std::string(globalArgs.pathPrefix);

        if(globalArgs.order == 0)
        {
            composeNormalizeComputePosteriorsAllNGrams(pathPrefix,
                    globalArgs.compose, globalArgs.normalize, globalArgs.save,
                    globalArgs.compile, globalArgs.acceptor);
        }
        else
        {
            composeNormalizeComputePosteriors(globalArgs.order, pathPrefix,
                    globalArgs.compose, globalArgs.normalize, globalArgs.save,
                    globalArgs.compile, globalArgs.acceptor);        
        }
    }
    else
    {
        string pathPrefix = "../fst-test/ncode-sample/bbc.2008.09.29.126145.en.fte.utf8.GRAPH-11";
//        pathPrefix = pathPrefix + ".bg";

        composeNormalizeComputePosteriorsAllNGrams(pathPrefix, true, true, true,
                true, false);
        
//        composeNormalizeComputePosteriors(2, pathPrefix, true, true, true,
//                true, false);
        
//        composeNormalizeComputePosteriors(2, pathPrefix, false, false, false,
//                false, false);        
    }

    return 0;
}
