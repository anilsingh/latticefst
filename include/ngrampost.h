/* 
 * File:   ngrampost.h
 * Author: Francois Yvon and Anil Kumar Singh
 *
 * Created on December 6, 2012, 9:11 AM
 */

#ifndef NGRAMPOST_H
#define	NGRAMPOST_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <omp.h>
#include <math.h>
#include <string>
#include <vector>

#include <fst/fst.h>
#include <fst/fstlib.h>
#include <fst/mutable-fst.h>
#include <fst/script/fstscript.h>

using namespace std;
using namespace fst;
using namespace fst::script;

//namespace s = fst::script;

//using fst::istream;
//using fst::ifstream;
using fst::SymbolTable;
        
typedef map<int, LogWeight> AMap;
typedef map<int, AMap> NMap;    
    
typedef AMap::iterator AMapItr;
typedef NMap::iterator NMapItr;

bool readOrCompileFST(VectorFst<LogArc> **logFst, string& pathOrPrefix,
        bool compile, bool acceptor);

bool compileFST(string& pathPrefix, bool acceptor);

bool composeNormalizeComputePosteriorsAllNGrams(string& pathPrefix, bool compose,
        bool normalize, bool save, bool compile, bool acceptor);

bool composeNormalizeComputePosteriors(int order, string& pathPrefix, bool compose,
        bool normalize, bool save, bool compile, bool acceptor);

bool composeNormalizeComputePosteriors(VectorFst<LogArc>& latticeFst, int order, string& pathPrefix,
        bool compose, bool normalize, bool save, bool compile, bool acceptor);

bool composeNormalize(VectorFst<LogArc>& latticeFst,
        VectorFst<LogArc>& ngramFst, VectorFst<LogArc>& normalizedFst, string& pathPrefix, bool save);

bool normalizeFST(VectorFst<LogArc>& ngramLatticeFst, VectorFst<LogArc>& normalizedFst);

int computePosteriors(VectorFst<LogArc>& normalizedFst, AMap& alphas, NMap& ngramAlphas,
        AMap& posteriors);

int saveResults(AMap& alphas, NMap& ngramAlphas, AMap& posteriors,
        VectorFst<LogArc>& normalizedFst, string& pathPrefix, bool saveNormFst);

int getNumArcs(VectorFst<LogArc>& fst, bool output);

int printPosteriors(AMap& posteriors, VectorFst<LogArc>& normalizedFst, string& pathPrefix);

int printAlphas(AMap& alphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix);

int printNGramAlphas(NMap& ngramAlphas, VectorFst<LogArc>& normalizedFst, string& pathPrefix);

string getPathPrefix(string& pathPrefix, int order);

#endif	/* NGRAMPOST_H */

