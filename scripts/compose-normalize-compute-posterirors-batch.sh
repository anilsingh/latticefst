#!/bin/sh

. scripts/compose.sh
. scripts/normalize.sh
. scripts/compose-normalize.sh

#composeFSTs "$1" "$2"
#composeNormalizeFST "$1" "$2"

IFILES=/vol/work/anil/trace-qe-sentence-level/lattices/en/fst-format/softi/*.fst.txt
#IFILES=/people/anil/work/trace-qe-sentence-level/lattices/en/fst-format/wmt/*.fst.txt
#IFILES=/people/anil/work/confidence-estimation/extracted-features/benjamin/lattices/lattices-fst/*.fst.txt

echo $IFILES

for f in $IFILES
do
  if ! echo "$f" | egrep -q ".composed.fst.txt" ; then
    if ! echo "$f" | egrep -q ".bg.fst.txt" ; then
      if ! echo "$f" | egrep -q ".tg.fst.txt" ; then
        if ! echo "$f" | egrep -q ".fg.fst.txt" ; then
          if ! echo "$f" | egrep -q ".norm.fst.txt" ; then
            dn=`dirname $f`
            bn=`basename $f .fst.txt`
            echo "Processing file $f ..."
				CMD="./dist/Release/GNU-Linux-x86/latticefst -t -o 0 -c -n -s -p $dn/$bn"
				eval "$CMD"
          fi
        fi
      fi
    fi
  fi
done
