#!/bin/sh

#export LD_LIBRARY_PATH=/usr/local/lib

LIBDIR="/usr/local/lib/fst"

mkdir bin

rm bin/latticefst

g++ -c -O2 -I$LIBDIR -Iinclude -MMD -MP -MF bin/ngrampostmain.o.d -o bin/ngrampostmain.o src/ngrampostmain.cpp

g++ -c -O2 -I$LIBDIR -Iinclude -MMD -MP -MF bin/ngrampost.o.d -o bin/ngrampost.o src/ngrampost.cpp

g++ -o bin/latticefst bin/ngrampostmain.o bin/ngrampost.o -lfst -ldl -lm -lpthread -lfstscript

rm bin/ngrampostmain.o.d bin/ngrampost.o.d bin/ngrampost.o bin/ngrampostmain.o
